import React from "react";

export const DefaultLayout = ({ children }) => {
  return (
    <div className="h-screen w-screen flex p-10">
      {children}
    </div>
  )
}