import React from "react";

export const PhotoWrapper = () => {
  return (
    <div className="flex max-w-[684px] w-full">
      <div className="flex p-5 rounded-[45px] border border-black">
        <img src="/img/owner.png" alt="" />
      </div>
    </div>
  )
}