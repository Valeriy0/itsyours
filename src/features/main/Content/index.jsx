import Link from "next/link";
import React from "react";

export const Content = () => {
  return (
    <div className="flex flex-col justify-between items-center max-w-[776px] w-full">
      <div className="">
        <span className="font-mirra text-5xl">Познай свои грани</span>
      </div>
      <div className="flex flex-1 items-center justify-center">
        <div className="rounded-[70px] border border-black p-10 font-mirra text-2xl flex flex-col text-center"> 
          <span>Сертификат на прохождение одной трансформационной игры:</span> 
          <span>„Лила”</span> 
          <span>„Территория денег”</span>
          <span>„Слияние душ”</span>
        </div>
      </div>
      <div className="flex flex-col justify-center items-center font-montserrat space-y-10">
        <span>
          Забронировать дату вашей игры можно на сайте
        </span>
        <Link className="py-[30px] px-[50px] border border-black rounded-[79px]" href='/'>
          Забронировать
        </Link>
      </div>
    </div>
  );
}