import React from "react";
import { PhotoWrapper, Content } from "features/main";

const Index = () => {
  return (
    <div className="flex justify-center w-full space-x-12">
      <PhotoWrapper />
      <Content />
    </div>
  );
}

export default Index;